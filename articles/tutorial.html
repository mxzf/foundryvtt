<h2>Overview</h2>
<p>Foundry Virtual Tabletop is a powerful application with a lot of features, so it can be overwhelming to a new game-master, even if you have previous experience using virtual tabletop software. This article provides a basic introduction to the layout of Foundry VTT and gives you all of the beginning steps required for setting up a game for your players. This article will introduce you to:</p>
<dl>
<dt>Entities</dt>
<dd>Foundry VTT stores its data in collections of documents categorized by what that data represents. Maps and locations are stored as @Article[scenes], characters and creatures are @Article[actors], objects, equipment, spells and other miscelaneous things as @Article[items], as well as lore and information @Article[journal]. There are also ambient sounds and music are @Article[playlists], and archival packages called @Article[compendium].</dd>
<dt>Game Worlds</dt>
<dd>Each Game World encapsulates the entities that it contains, and also the accounts and information for player and GM accounts, game worlds also require game worlds provided by Game Systems which define the aspects of a game that are necessary play it. Whether the game you are playing calls them campaigns, chronicles, stories, or groups, the term Foundry VTT refers to them as Game Worlds.</dd>
<dt>Game Systems</dt>
<dd>A Game System contains all the rules and specific user interface elements for your game, and also defines how data is structured and stored. Foundry VTT's game systems are provided primarily by the community, with a wide variety of different games supported. <a href="https://foundryvtt.com/packages/systems">See a list of currently available Game Systems</a>.</dd>
</dl>
<p>In addition, this article will also give you all the steps required to create a Game World, how to configure it, and and how to launch the game world so that you can prepare your content for your players and allow them to connect.</p>
<h4>Recommended Action: Set your Admin Password</h4>
<p class="note warning">When you first launch Foundry Virtual Tabletop, it's recommended that you navigate to the Configuration tab and set an <strong>Administrator Password</strong>. This password is encrypted and allows you to secure Foundry VTT to prevent access to the main setup menu. It's also required to use the <strong>Return to Setup</strong> feature from the login page of any currently hosted world.</p>
<h2 class="border">Installing a Game System</h2>
<figure class="right"><a title="See Larger Image" href="https://foundryvtt.s3.us-west-2.amazonaws.com/website-media-dev/user_1294/screen/the-game-system-browser-2021-01-20.jpg" target="_blank" rel="nofollow noopener">@Image[55]</a>
<figcaption>Game Systems can be easily installed using the Module Browser.</figcaption>
</figure>
<p>Before you can create your first world you will first need to install a <strong>Game System</strong>. Game Systems set the rules by which your world operates, whether this is one of the more common rulesets from a major publisher, or if you prefer to use something like the Simple Worldbuilding System. Each world has a <strong>Game System</strong> tied to it. Without one installed, it is impossible to create a world.</p>
<p>Foundry VTT provides a package installation system available from the Setup screen for the installation of <strong>Game Worlds</strong>, <strong>Game Systems</strong>, and <strong>Add-on Modules</strong>. The "Install System" button at the bottom of each of these tabs on the Setup screen will allow you to install the package type for that tab.</p>
<h4>The Game System Installation Menu</h4>
<ol>
<li>From the <strong>Setup</strong> screen navigate to the Game Systems tab</li>
<li>Click the "<strong>Install System</strong>" button at the bottom of the menu</li>
<li>A package installation browser will appear allowing you to see all of the Game Systems currently available for Foundry VTT</li>
<li>You can search this listing with the search field, or filter the listing using package categories</li>
<li>Once you have chosen a Game System, click the install button to the right of the system's name, and FVTT will download and install it for you</li>
</ol>
<p>Game Systems which have not yet been officially released can also be installed manually if you have a link to the <code>system.json</code> file (called the <strong>manifest</strong> url) for that system, by pasting the URL into the Manifest URL field and clicking the install button.</p>
<h4>Updating Game Systems</h4>
<p>As a reminder, it is wise to periodically update installed systems from the Game System tab of the main Foundry VTT menus. You can update game systems individually by clicking the associated "check update" button in the system's entry, or use the "update all" button at the bottom of the tab to check all of your installed systems for updates, which will then be automatically applied if one is available.</p>
<h4>About Modules</h4>
<p>It is a common pitfall for some users to rush immediately to install Add-on Modules without first learning to use the basic features of Foundry VTT. While @Article[modules] provide a large variety of customization and changes to the way core Foundry VTT features work, they can carry with them some compatibility issues and should be installed in small increments to allow you to be certain which features were added by community-supported modules and which features are part of the core software.</p>
<p>Always remember, the first step in troubleshooting any issue you may experience in Foundry VTT is to disable all modules.</p>
<h2 class="border">Creating A New World</h2>
<figure class="right" style="width: 30%;"><a title="See Larger Image" href="https://foundryvtt.s3.us-west-2.amazonaws.com/website-media-dev/user_1/screen/the-world-configuration-sheet-2020-05-23.png" target="_blank" rel="nofollow noopener">@Image[56]</a>
<figcaption>The World configuration sheet allows you to specify the details of your Game World and edit them later.</figcaption>
</figure>
<p>Now that you have a <strong>Game System</strong>, navigate to the <strong>Game Worlds</strong> tab, from here you will create your very first Game World! Clicking the "Create World" button on this tab presents a dialog menu.</p>
<h4>Editing an Existing World</h4>
<p>The <strong>Edit Game World</strong> button can be used at any time after a world has been created, allowing you to display this menu again in order to change details about your game world such as the description, next session, background image, or title! It can also be used to <strong>reset active modules</strong> or <strong>reset user access keys</strong>without having to launch the world first. If you are having trouble launching your world or logging in, simply edit the world and check the appropriate box to reset modules or user access keys as necessary.</p>
<h4>The Create World Menu</h4>
<dl>
<dt>World Title</dt>
<dd>The title that players will see when logging in to the game, and how the world will be referred to in the user interface.</dd>
<dt>Data Path</dt>
<dd>The folder name within the Worlds folder where your data will be stored. As this will be used in web URLs, it <strong>cannot contain spaces or special characters</strong>, use hyphens to separate multiple terms instead. (Example: <strong>my-first-campaign</strong> is better than <strong>"my first campaign"</strong>.)</dd>
<dt>Game System</dt>
<dd>The ruleset the world will use. A game system cannot be changed once the world is created, so be sure you are using the correct system.</dd>
<dt>Background Image</dt>
<dd>An image that you and your players will see when logging in to the Game World for a session. This image will be stretched to fit the browser window, so it is recommended that you use an image that is at least large enough to function as a desktop wallpaper.</dd>
<dt>Next Session</dt>
<dd>Allows you to set the date and time for the next game session, and will be visible to any players from the login screen for your Game World. This is purely optional. The date and time is automatically localized to the user's correct timezone.</dd>
<dt>World Description</dt>
<dd>Provides a text description for your world, allowing for some additional thematic information or a brief description for your setting, current plot points, or other information your players will see when logging in.</dd>
</dl>
<p>Once you have filled out these fields to your liking, click Create World to finalize your Game World!</p>
<h2 class="border">Installing a Game World</h2>
<p>Foundry VTT offers a number of adventures and campaigns which come already prepared with everything you need to run them with your players. Similar to the installation of Game Systems, these worlds can be installed from the Game Worlds tab and will automatically install all content, including the Game System for you.</p>
<h4>The Game World Installation Menu</h4>
<ol>
<li>From the<strong>Setup</strong>screen navigate to the Game Systems tab</li>
<li>Click the "<strong>Install World</strong>" button at the bottom of the menu</li>
<li>A package installation browser will appear allowing you to see all of the Game Worlds currently available for Foundry VTT</li>
<li>You can search this listing with the search field, or filter the listing using package categories</li>
<li>Once you have chosen a Game World, click the install button to the right of the World's Title, and FVTT will download and install it for you</li>
</ol>
<p>As with Game Systems, Game Worlds which have not yet been officially released can also be installed manually using the <code>world.json</code> Manifest URL field.</p>
<h2 class="border">Launching Your New World</h2>
<p>Once your Game World has been created, simply clicking the "Launch World" button to the right of the world's Title will launch it the world and take you to the game's login screen. From here you can select the username to log in as (by default there is initially only a single, passwordless gamemaster account) or return to the setup menu.</p>
<p>Select the gamemaster player, and log in.</p>
<p>Once you have joined the game session, you'll see the core user interface which you and your players will use to plan and play games in FVTT. The image below details the major elements.</p>
<figure><a href="https://foundryvtt.s3.us-west-2.amazonaws.com/website-media-dev/user_671/screen/gm-ui-map-2021-01-27.jpg">@Image[58]</a>
<figcaption>This UI map describes the primary interface elements you will first encounter in a newly created world.</figcaption>
</figure>
<dl>
<dt>Canvas</dt>
<dd>The canvas, not labeled in the picture to the right, is the underlying layer where the scene and all objects are shown.</dd>
<dt>Game Pause Indicator</dt>
<dd>When the game is paused, players may not move their tokens or activate doors. You can toggle the paused state of the game by pressing the <code>space</code> as GM at any time.</dd>
<dt>Layer Controls</dt>
<dd>Used to switch layers on your @Article[scenes] to control the various placeable canvas objects, including: Tokens, Measured Templates, Tiles, Drawings, Walls, Lights, Sounds, and Map Notes.</dd>
<dt>Layer Tools</dt>
<dd>The specific tools related to the layer you are currently using.</dd>
<dt>Scene Navigation Bar</dt>
<dd>Used to switch between currently available scenes. Activatating a scene will display it for all users, simply clicking on a scene will display it only for you.</dd>
<dt>Sidebar Tabs</dt>
<dd>Used to access data for the various entities stored in your world.</dd>
</dl>
<ul>
<li><strong>Chat Log</strong> displays current chat messages and dice rolls</li>
<li><strong>Combat Tracker</strong> displays any currently active combats</li>
<li><strong>Scenes</strong> allow you to create new scenes or switch between scenes that are currently available</li>
<li><strong>Actors</strong> are commonly used to store and access character sheets</li>
<li><strong>Items</strong> store information sheets for inventory and equipment, but also are commonly used for spells and abilities</li>
<li><strong>Journals</strong> store image and text handouts which can be viewed and edited by yourself or shown to your players as necessary</li>
<li><strong>Tables</strong> can be used to roll for determining randomized outcomes from a list of results</li>
<li><strong>Playlists</strong> control audio that can be played for all users to hear</li>
<li><strong>Compendium Packs</strong> are used to store entities that are not actively needed</li>
<li><strong>Settings</strong> allows for the configuration or customization of Foundry VTT as well as adding users to your game world</li>
</ul>
<dl>
<dt>Players List</dt>
<dd>Displays the current user accounts, when collapsed, only shows the users who are currently signed in.</dd>
<dt>Macro Directory</dt>
<dd>Provides access to all macros that have been created which you have permission to view, use, or edit.</dd>
<dt>Macro Hotbar</dt>
<dd>Gives instant keypress access to 10 quick Macros, which can be used to send chat messages or perform scripted API actions. Primarily used by advanced users. Some systems provide the ability to drop items or actors onto the macro bar to generate a macro for quick use. The Hotbar Pages control can be used to switch between up to four different pages of macro layouts.</dd>
<dt>Default Roll Mode</dt>
<dd>used to set the current mode to be used for rolling dice. For more information see Rolling Dice.</dd>
<dt>Export Chat Log</dt>
<dd>used to save a copy of all messages which presently appear in your chat log to a plain text file.</dd>
<dt>Clear Chat Log</dt>
<dd>deletes all messages from the chat log.</dd>
<dt>Chat Entry</dt>
<dd>used to chat, communicate, and enter chat commands.</dd>
</dl>
<h4>Conclusion of Part 1</h4>
<p>This concludes part one of the Getting Started Tutorial!</p>
<ul>
    <li>Continue to @Article[tutorial-part-two].</li>
    <li>Refer your players to the @Article[player-orientation].</li>
</ul>