<h2>Overview</h2>
<p>Foundry Virtual Tabletop supports the use of audio to accentuate player actions or enrich the atmosphere during gaming sessions.</p>
<p>This page details the use of Playlists, the sounds contained within them, and the playlist's various settings.</p>
<h2 id="playlistdirectory" class="border">The Playlist Directory</h2>
<p>The Playlist Directory allows Gamemasters and Assistants to create new Playlists and modify existing ones. All players can access this sidebar, to view the currently playing playlists and adjust client-side volume controls.</p>
<h3 id="controls">Global Volume Controls</h3>
<p>Global volume controls are client-sided sliders that modify all sounds of certain categories, allowing players to fine-tune volume levels on their end.</p>
<p>The <strong>Playlists</strong> volume slider determines the master volume of playlists played by the Gamemaster.</p>
<p>The <strong>Ambient</strong> volume slider adjusts the master volume of Ambient Sounds heard in a Scene.</p>
<p>The <strong>Interface</strong> volume slider adjusts the master volume of sounds triggered through interface actions (such as chat messages and dice rolls).</p>
<h3 id="create">Creating Playlists</h3>
<p>In order to play audio tracks on demand, or as a sequence, they must be first added to a playlist. A playlist can contain multiple sounds, and the same sound can be in multiple playlists.</p>
<p>Click the "Create Playlist" button at the bottom of the Playlist Directory sidebar to create a playlist. From there, a prompt will appear, allowing you to enter the name of the playlist.</p>
<p class="note info">Playlists are added to the bottom of the list, but are sorted in alphabetical order once Foundry is refreshed.</p>
<h3 id="manage">Managing Playlists</h3>
<figure class="right"><a href="https://foundryvtt.s3.us-west-2.amazonaws.com/website-media-dev/user_1/screen/playlist-sidebar-2020-05-22.png" alt="The Playlist Directory Controls">@Image[43]</a>
<figcaption>The Gamemaster's view of the Playlist Directory sidebar.</figcaption>
</figure>
<p>Playlists on their own come with various buttons and toggles used to manage both the playlist itself and the playback of the sounds it contains.</p>
<dl>
<dt>Edit Playlist</dt>
<dd>Opens a prompt to edit the playlist's name and its Playback Mode</dd>
<dt>Add Sound</dt>
<dd>Adds a Sound to the playlist. See the "Adding Sounds" section in this article.</dd>
<dt>Delete Playlist</dt>
<dd>Deletes the playlist after accepting a confirmation prompt.</dd>
<dt>Playback Mode</dt>
<dd>Cycles through the various playback modes. Playback Mode determines the behavior of the playlist when the "Play Playlist" button is pressed.<br /><strong>Sequential Playback:</strong> Plays each sound in the playlist, one at a time, in order.<br /><strong>Shuffle Tracks:</strong> Plays each sound in the playlist, one at a time, in a random order.<br /><strong>Simultaneous Playback:</strong> Plays each sound in the playlist at the same time.<br /><strong>Soundboard Only: </strong> Does not allow the playlist to be played as a whole. The playlist will always be expanded when Foundry reloads, and each sound can be played separately as normal.</dd>
<dt>Play/Stop Playlist</dt>
<dd>Plays the tracks in the playlist according to the Playlist Mode. If stopping, all currently playing tracks in the playlist will stop playing.</dd>
</dl>
<p class="note info">Normally, only Gamemasters and Assistant users can view and manage playlists, but all players can see the names of playlists when they are played. Take care to keep your playlist and sound names spoiler-free!</p>
<h3 id="add">Adding Sounds</h3>
<figure class="right"><a href="https://foundryvtt.s3.us-west-2.amazonaws.com/website-media-dev/user_1294/screen/the-playlist-sound-configuration-sheet-2021-02-15.jpg" alt="The Playlist Sound Configuration Sheet">@Image[44]</a>
<figcaption>The New Track configuration window.</figcaption>
</figure>
<p>Once a playlist has been created, individual sounds can be added to it by clicking the "Add Sound" button next to the playlist (notated by a plus sign icon). This will bring up the configuration window allowing you to add the sound to the chosen playlist. Once a sound has been created in a playlist, the playlist can be expanded to manage the individual sound, such as editing the name or sound source, toggling the track to loop when played, or deleting the sound from the playlist.</p>
<dl>
<dt>Track Name</dt>
<dd>The name of the sound, as seen in the Playlists Directory. This is the name all players see in the Playlists Directory sidebar while the sound is playing.</dd>
<dt>Audio Source</dt>
<dd>The source file for the sound. This can either be a file stored in Foundry directories, or a direct URL link to a sound file online. FVTT supports .flac, .mp3, .wav, and .webm audio files.</dd>
<dt>Volume</dt>
<dd>The volume of the individual sound. The sound's volume is also affected by the Master Playlists volume slider.</dd>
<dt>Repeat</dt>
<dd>Determines whether the sound repeats when played. Note that currently, sounds do not loop seamlessly - there will be a short gap between loops, regardless of the sound file chosen.</dd>
<dt>Large File Streaming</dt>
<dd>Determines whether the sound should be streamed to players instead of using HTML5 audio. This toggle is meant for audio files greater than 5MB, as HTML5 Audio is not ideal for large audio files due to various technical reasons. This function will very likely be removed after the upcoming 0.8.1 update in favour of a better implementation of audio play.</dd>
</dl>
<h2 id="playlistapi" class="border">API References</h2>
<p>To interact with Playlists programmatically, consider using the following API concepts:</p>
<ul>
<li>The <a title="The Playlist Entity" href="/api/Playlist.html" target="_&quot;blank&quot;/">Playlist</a> Entity</li>
<li>The <a title="The Playlists Collection" href="/api/Playlists.html" target="_&quot;blank&quot;/">Playlists</a> Collection</li>
<li>The <a title="The PlaylistDirectory Sidebar Directory" href="/api/PlaylistDirectory.html" target="_&quot;blank&quot;/">PlaylistDirectory</a> Sidebar Directory</li>
<li>The <a title="The PlaylistConfig Application" href="/api/PlaylistConfig.html" target="_&quot;blank&quot;/">PlaylistConfig</a> Application</li>
<li>The <a title="The PlaylistSoundConfig Application" href="/api/PlaylistSoundConfig.html" target="_&quot;blank&quot;/">PlaylistSoundConfig</a> Application</li>
</ul>
<hr />